package ru.volkova.tm.api.service;

import javax.jms.Message;

public interface ILogService {

    void writeLog(final Message message);

}
