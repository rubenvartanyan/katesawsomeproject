package ru.volkova.tm.listener;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.volkova.tm.api.service.ILogService;
import ru.volkova.tm.service.LogService;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

@AllArgsConstructor
public class LogMessageListener implements MessageListener {

    @NotNull
    final ILogService loggingService = new LogService();

    @Override
    public void onMessage(Message message) {
        if (message instanceof ObjectMessage) {
            loggingService.writeLog(message);
        }
    }

}

