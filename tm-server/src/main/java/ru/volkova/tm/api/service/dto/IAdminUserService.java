package ru.volkova.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.dto.User;
import ru.volkova.tm.enumerated.Role;

import java.util.List;

public interface IAdminUserService extends IService<User> {

    void clear();

    @NotNull
    User add(@Nullable final User user);

    void addAll(@Nullable List<User> entities);

    @NotNull
    User createUser(@Nullable final String login, @Nullable final String password);

    @NotNull
    User createUserWithEmail(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    );

    @NotNull
    User createUserWithRole
            (@Nullable final String login,
             @Nullable final String password,
             @Nullable final Role role);

    @NotNull
    List<User> findAll();

    @Nullable
    User findByEmail(@Nullable final String email);

    @Nullable
    User findById(@Nullable final String id);

    @Nullable
    User findByLogin(@Nullable final String login);

    boolean isEmailExists(@Nullable final String email);

    boolean isLoginExists(@Nullable final String login);

    void lockByEmail(@Nullable String email);

    void lockById(@Nullable String id);

    void lockByLogin(@Nullable String login);

    void removeByEmail(@Nullable String email);

    void removeById(@Nullable final String id);

    void removeByLogin(@Nullable final String login);

    void unlockByEmail(@Nullable String email);

    void unlockById(@Nullable String id);

    void unlockByLogin(@Nullable String login);

    void updateUser(
            @NotNull final String userId,
            @Nullable final String firstName,
            @Nullable final String secondName,
            @Nullable final String middleName
    );

}
