package ru.volkova.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.model.TaskGraph;

import java.util.List;

public interface IProjectTaskServiceGraph {

    void bindTaskByProjectId(
            @NotNull String userId,
            @Nullable String projectId,
            @NotNull String taskId
    );

    @NotNull
    List<TaskGraph> findAllTasksByProjectId(@NotNull String userId, @Nullable String projectId);

    void removeProjectById(@NotNull String userId, @Nullable String id);

    void unbindTaskByProjectId(
            @NotNull String userId,
            @Nullable String projectId,
            @NotNull String taskId
    );

}
