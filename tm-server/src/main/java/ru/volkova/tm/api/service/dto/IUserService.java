package ru.volkova.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.dto.User;

public interface IUserService extends IService<User> {

    void setPassword(@NotNull String userId, @Nullable String password);

}
