package ru.volkova.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.volkova.tm.api.repository.IRepositoryGraph;
import ru.volkova.tm.model.SessionGraph;

public interface ISessionRepositoryGraph extends IRepositoryGraph<SessionGraph> {

    void add(@NotNull SessionGraph sessionGraph);

    void close(@NotNull SessionGraph sessionGraph);

}
