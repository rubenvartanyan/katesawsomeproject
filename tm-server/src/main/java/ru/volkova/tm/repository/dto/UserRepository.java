package ru.volkova.tm.repository.dto;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.repository.dto.IUserRepository;
import ru.volkova.tm.dto.User;
import ru.volkova.tm.exception.entity.UserNotFoundException;

import javax.persistence.EntityManager;
import java.util.List;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public UserRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    @NotNull
    public User insert(@Nullable User user) {
        if (user == null) throw new UserNotFoundException();
        entityManager.persist(user);
        return user;
    }

    @Override
    public void clear() {
        entityManager.createQuery("DELETE FROM user t", User.class);
    }

    @Override
    @NotNull
    public List<User> findAll() {
        return entityManager
                .createQuery("SELECT t FROM user t", User.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .getResultList();
    }

    @Override
    @Nullable
    public User findByEmail(@NotNull String email) {
        return entityManager
                .createQuery("SELECT t FROM user t WHERE t.email = :email", User.class)
                .setParameter("email", email)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    public User findById(@NotNull String id) {
        return entityManager
                .createQuery("SELECT t FROM user t WHERE t.id = :id", User.class)
                .setParameter("id", id)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    public User findByLogin(@NotNull String login) {
        return entityManager
                .createQuery("SELECT t FROM user t WHERE t.login = :login", User.class)
                .setParameter("login", login)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    public void lockByEmail(@NotNull String email) {
        entityManager
                .createQuery("UPDATE user t SET t.locked = :locked" +
                                "WHERE t.email = :email", User.class)
                .setParameter("email", email)
                .setParameter("locked", false)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .executeUpdate();
    }

    @Override
    public void lockById(@NotNull String id) {
        entityManager
                .createQuery("UPDATE user t SET t.locked = :locked" +
                        "WHERE t.id = :id", User.class)
                .setParameter("id", id)
                .setParameter("locked", false)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .executeUpdate();
    }

    @Override
    public void lockByLogin(@NotNull String login) {
        entityManager
                .createQuery("UPDATE user t SET t.locked = :locked" +
                        "WHERE t.login = :login", User.class)
                .setParameter("login", login)
                .setParameter("locked", false)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .executeUpdate();
    }

    @Override
    public void removeByEmail(@NotNull String email) {
        entityManager
                .createQuery("DELETE FROM user t WHERE t.email = :email",
                        User.class)
                .setParameter("email", email)
                .setHint(QueryHints.HINT_CACHEABLE, true);
    }

    @Override
    public void removeById(@NotNull String id) {
        entityManager
                .createQuery("DELETE FROM user t WHERE t.id = :id",
                        User.class)
                .setParameter("id", id)
                .setHint(QueryHints.HINT_CACHEABLE, true);
    }

    @Override
    public void removeByLogin(@NotNull String login) {
        entityManager
                .createQuery("DELETE FROM user t WHERE t.login = :login",
                        User.class)
                .setParameter("login", login)
                .setHint(QueryHints.HINT_CACHEABLE, true);
    }

    @Override
    public void unlockByEmail(@NotNull String email) {
        entityManager
                .createQuery("UPDATE user t SET t.locked = :locked" +
                        "WHERE t.email = :email", User.class)
                .setParameter("email", email)
                .setParameter("locked", true)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .executeUpdate();
    }

    @Override
    public void unlockById(@NotNull String id) {
        entityManager
                .createQuery("UPDATE user t SET t.locked = :locked" +
                        "WHERE t.id = :id", User.class)
                .setParameter("id", id)
                .setParameter("locked", true)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .executeUpdate();
    }

    @Override
    public void setPassword(@NotNull String userId, @Nullable String hash) {
        entityManager
                .createQuery("UPDATE user t SET t.password_hash = :passwordHash" +
                        "WHERE t.id = :id", User.class)
                .setParameter("passwordHash", hash)
                .setParameter("id", userId)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .executeUpdate();
    }

    @Override
    public void unlockByLogin(@NotNull String login) {
        entityManager
                .createQuery("UPDATE user t SET t.locked = :locked" +
                        "WHERE t.login = :login", User.class)
                .setParameter("login", login)
                .setParameter("locked", true)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .executeUpdate();
    }

    @Override
    public void updateUser(
            @NotNull String userId,
            @Nullable String firstName,
            @Nullable String secondName,
            @Nullable String middleName
    ) {
        entityManager
                .createQuery("UPDATE user t SET t.first_name = :firstName, " +
                                "t.second_name = :secondName, t.middle_name = :middleName" +
                                "WHERE t.id = :id", User.class)
                .setParameter("firstName", firstName)
                .setParameter("secondName", secondName)
                .setParameter("middleName", middleName)
                .setParameter("id", userId)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .executeUpdate();
    }

}
