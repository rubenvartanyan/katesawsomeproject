package ru.volkova.tm.repository.dto;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.repository.dto.ITaskRepository;
import ru.volkova.tm.dto.Task;
import ru.volkova.tm.enumerated.Status;
import ru.volkova.tm.exception.entity.TaskNotFoundException;

import javax.persistence.EntityManager;
import java.util.List;

public class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    public TaskRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    @Nullable
    public Task insert(@Nullable Task task) {
        if (task == null) throw new TaskNotFoundException();
        entityManager.persist(task);
        return task;
    }

    @Override
    public void clear(@NotNull String userId) {
        entityManager
                .createQuery("DELETE FROM task t WHERE t.user_id = :userId")
                .setParameter("userId", userId)
                .setHint(QueryHints.HINT_CACHEABLE, true);
    }

    @Override
    @NotNull
    public List<Task> findAll(@NotNull String userId) {
        return entityManager
                .createQuery("SELECT t FROM task t", Task.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .getResultList();
    }

    @Override
    public void bindTaskByProjectId(
            @NotNull String userId,
            @NotNull String projectId,
            @NotNull String taskId
    ) {
        entityManager
                .createQuery("UPDATE task t SET t.project_id = :projectId" +
                                "WHERE t.user_id = :userId AND t.id = :id ",
                        Task.class)
                .setParameter("userId", userId)
                .setParameter("id", taskId)
                .setParameter("projectId", projectId)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .executeUpdate();
    }

    @Override
    @NotNull
    public List<Task> findAllByProjectId(@NotNull String userId, @NotNull String projectId) {
        return entityManager
                .createQuery("SELECT t FROM task t " +
                        "WHERE t.project_id = :projectId", Task.class)
                .setParameter("projectId", projectId)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .getResultList();
    }

    @Override
    public void removeAllByProjectId(@NotNull String userId, @NotNull String projectId) {
         entityManager
                 .createQuery("DELETE t FROM task t " +
                        "WHERE t.project_id = :projectId", Task.class)
                 .setParameter("projectId", projectId)
                 .setHint(QueryHints.HINT_CACHEABLE, true);
    }

    @Override
    public void unbindTaskByProjectId(@NotNull String userId, @NotNull String projectId, @NotNull String taskId) {
        entityManager
                .createQuery("UPDATE task t SET t.project_id = :projectId" +
                                "WHERE t.user_id = :userId AND t.id = :id ",
                        Task.class)
                .setParameter("userId", userId)
                .setParameter("id", taskId)
                .setParameter("projectId", null)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .executeUpdate();
    }

    @Override
    @Nullable
    public Task findById(@NotNull String userId, @NotNull String id) {
        return entityManager
                .createQuery("SELECT t FROM task t " +
                                "WHERE t.user_id = :userId AND t.id = :id",
                        Task.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    public Task findOneByIndex(@NotNull String userId, @NotNull Integer index) {
        return entityManager
                .createQuery("SELECT t FROM task t WHERE t.user_id = :userId",
                        Task.class)
                .setParameter("userId", userId)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setFirstResult(index)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    public Task findOneByName(@NotNull String userId, @NotNull String name) {
        return entityManager
                .createQuery("SELECT t FROM task t " +
                                "WHERE t.user_id = :userId AND t.name = :name",
                        Task.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    public void changeOneStatusById(
            @NotNull String userId,
            @Nullable String id,
            @Nullable Status status
    ) {
        entityManager
                .createQuery("UPDATE task t SET t.status = :status" +
                                "WHERE t.user_id = :userId AND t.id = :id",
                        Task.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setParameter("status", status)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .executeUpdate();
    }

    @Override
    public void changeOneStatusByName(
            @NotNull String userId,
            @Nullable String name,
            @Nullable Status status
    ) {
        entityManager
                .createQuery("UPDATE task t SET t.status = :status" +
                                "WHERE t.user_id = :userId AND t.name = :name",
                        Task.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .setParameter("status", status)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .executeUpdate();
    }

    @Override
    public void removeById(@NotNull String userId, @NotNull String id) {
        entityManager
                .createQuery("DELETE FROM task t " +
                        "WHERE t.user_id = :userId and t.id = :id", Task.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .executeUpdate();
    }

    @Override
    public void removeOneByName(@NotNull String userId, @NotNull String name) {
        entityManager
                .createQuery("DELETE FROM task t " +
                        "WHERE t.user_id = :userId and t.name = :name", Task.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .executeUpdate();
    }

    @Override
    public void updateOneById(
            @NotNull String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    ) {
        entityManager
                .createQuery("UPDATE task t SET t.name = :name, t.description = :description" +
                                "WHERE t.user_id = :userId AND t.id = :id",
                        Task.class)
                .setParameter("name", name)
                .setParameter("description", description)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .executeUpdate();
    }

}
