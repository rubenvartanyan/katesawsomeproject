package ru.volkova.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.service.IConnectionService;
import ru.volkova.tm.api.service.model.IProjectServiceGraph;
import ru.volkova.tm.model.ProjectGraph;
import ru.volkova.tm.enumerated.Status;
import ru.volkova.tm.exception.empty.EmptyNameException;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;
import ru.volkova.tm.exception.entity.ProjectNotFoundException;
import ru.volkova.tm.exception.entity.TaskNotFoundException;
import ru.volkova.tm.repository.model.ProjectRepositoryGraph;
import ru.volkova.tm.repository.model.UserRepositoryGraph;

import javax.persistence.EntityManager;
import java.util.List;

public final class ProjectServiceGraph extends AbstractServiceGraph<ProjectGraph> implements IProjectServiceGraph {

    public ProjectServiceGraph(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @SneakyThrows
    @Override
    public void insert(@Nullable final ProjectGraph projectGraph) {
        if (projectGraph == null) throw new ProjectNotFoundException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ProjectRepositoryGraph projectRepository = new ProjectRepositoryGraph(entityManager) ;
            projectRepository.insert(projectGraph);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void add(
            @NotNull String userId,
            @Nullable String name,
            @Nullable String description
    ) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new ObjectNotFoundException();
        @NotNull ProjectGraph projectGraph = new ProjectGraph();
        projectGraph.setName(name);
        projectGraph.setDescription(description);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final UserRepositoryGraph userRepository = new UserRepositoryGraph(entityManager);
            projectGraph.setUser(userRepository.findById(userId));
            entityManager.getTransaction().begin();
            @NotNull final ProjectRepositoryGraph projectRepository = new ProjectRepositoryGraph(entityManager) ;
            projectRepository.insert(projectGraph);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable List<ProjectGraph> entities) {
        if (entities == null) throw new TaskNotFoundException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ProjectRepositoryGraph projectRepository = new ProjectRepositoryGraph(entityManager) ;
            entities.forEach(projectRepository::insert);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clear(@NotNull String userId) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ProjectRepositoryGraph projectRepository = new ProjectRepositoryGraph(entityManager) ;
            projectRepository.clear(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<ProjectGraph> findAll(@NotNull String userId) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ProjectRepositoryGraph projectRepository = new ProjectRepositoryGraph(entityManager) ;
            final List<ProjectGraph> projectGraphs = projectRepository.findAll(userId);
            entityManager.getTransaction().commit();
            return projectGraphs;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public ProjectGraph findById(@NotNull String userId, @NotNull String id) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ProjectRepositoryGraph projectRepository = new ProjectRepositoryGraph(entityManager) ;
            final ProjectGraph projectGraph = projectRepository.findById(userId,id);
            entityManager.getTransaction().commit();
            return projectGraph;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public ProjectGraph findOneByIndex(@NotNull String userId, @NotNull Integer index) {
        if (index < 0) return null;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ProjectRepositoryGraph projectRepository = new ProjectRepositoryGraph(entityManager) ;
            final ProjectGraph projectGraph = projectRepository.findOneByIndex(userId,index);
            entityManager.getTransaction().commit();
            return projectGraph;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public ProjectGraph findOneByName(@NotNull String userId, @NotNull String name) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ProjectRepositoryGraph projectRepository = new ProjectRepositoryGraph(entityManager) ;
            final ProjectGraph projectGraph = projectRepository.findOneByName(userId,name);
            entityManager.getTransaction().commit();
            return projectGraph;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void changeOneStatusById(@NotNull String userId, @NotNull String id, @Nullable Status status) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ProjectRepositoryGraph projectRepository = new ProjectRepositoryGraph(entityManager) ;
            projectRepository.changeOneStatusById(userId, id, status);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void changeOneStatusByName(@NotNull String userId, @NotNull String name, @Nullable Status status) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ProjectRepositoryGraph projectRepository = new ProjectRepositoryGraph(entityManager) ;
            projectRepository.changeOneStatusByName(userId, name, status);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeById(@NotNull String userId, @NotNull String id) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ProjectRepositoryGraph projectRepository = new ProjectRepositoryGraph(entityManager) ;
            projectRepository.removeById(userId, id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeOneByName(@NotNull String userId, @NotNull String name) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ProjectRepositoryGraph projectRepository = new ProjectRepositoryGraph(entityManager) ;
            projectRepository.removeOneByName(userId, name);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void updateOneById(@NotNull String userId, @Nullable String id, @Nullable String name, @Nullable String description) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ProjectRepositoryGraph projectRepository = new ProjectRepositoryGraph(entityManager) ;
            projectRepository.updateOneById(userId, id, name, description);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
