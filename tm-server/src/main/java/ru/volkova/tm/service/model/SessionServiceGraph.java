package ru.volkova.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.service.IConnectionService;
import ru.volkova.tm.api.service.model.ISessionServiceGraph;
import ru.volkova.tm.api.service.ServiceLocator;
import ru.volkova.tm.enumerated.Role;
import ru.volkova.tm.exception.auth.AccessDeniedException;
import ru.volkova.tm.model.SessionGraph;
import ru.volkova.tm.model.UserGraph;
import ru.volkova.tm.repository.model.SessionRepositoryGraph;
import ru.volkova.tm.util.HashUtil;
import ru.volkova.tm.api.service.IPropertyService;

import javax.persistence.EntityManager;
import java.util.List;

public final class SessionServiceGraph extends AbstractServiceGraph<SessionGraph> implements ISessionServiceGraph {

    @NotNull
    private final ServiceLocator serviceLocator;

    public SessionServiceGraph(
            @NotNull final ServiceLocator serviceLocator,
            @NotNull final IConnectionService connectionService) {
        super(connectionService);
        this.serviceLocator = serviceLocator;
    }

    public void add(@NotNull SessionGraph sessionGraph) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final SessionRepositoryGraph sessionRepository = new SessionRepositoryGraph(entityManager) ;
            sessionRepository.add(sessionGraph);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public SessionGraph open(
            @Nullable final String login,
            @Nullable final String password
    ) {
        final boolean check = checkDataAccess(login, password);
        if (!check) throw new AccessDeniedException();
        @Nullable final UserGraph userGraph = serviceLocator.getAdminUserService().findByLogin(login);
        if (userGraph == null) return null;
        @NotNull final SessionGraph sessionGraph = new SessionGraph();
        sessionGraph.setUser(userGraph);
        @Nullable final SessionGraph signSessionGraph = sign(sessionGraph);
        if (signSessionGraph == null) return null;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final SessionRepositoryGraph sessionRepository = new SessionRepositoryGraph(entityManager) ;
            sessionRepository.add(sessionGraph);
            entityManager.getTransaction().commit();
            return sessionGraph;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean checkDataAccess(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (login == null || login.isEmpty()) return false;
        if (password == null || password.isEmpty()) return false;
        @Nullable final UserGraph userGraph = serviceLocator.getAdminUserService().findByLogin(login);
        if (userGraph == null) return false;
        final String passwordHash = HashUtil.salt(serviceLocator.getPropertyService(), password);
        if (passwordHash == null || passwordHash.isEmpty()) return false;
        final String passwordHash2 = userGraph.getPasswordHash();
        return passwordHash.equals(passwordHash2);
    }

    @Override
    @SneakyThrows
    public void validate(@Nullable final SessionGraph sessionGraph) {
        if (sessionGraph == null) throw new AccessDeniedException();
        final String signature = sessionGraph.getSignature();
        if (signature == null || signature.isEmpty()) throw new AccessDeniedException();
        if (sessionGraph.getTimestamp() == null) throw new AccessDeniedException();
        @Nullable final SessionGraph temp = sessionGraph.clone();
        if (temp == null) throw new AccessDeniedException();
        @NotNull final String signatureSource = sessionGraph.getSignature();
        @Nullable final SessionGraph sessionGraphTarget = sign(temp);
        if (sessionGraphTarget == null) throw new AccessDeniedException();
        @Nullable final String signatureTarget = sessionGraphTarget.getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
    }

    @Override
    @SneakyThrows
    public void validateAdmin(@Nullable final SessionGraph sessionGraph, @Nullable final Role role) {
        if (sessionGraph == null) throw new AccessDeniedException();
        if (role == null) throw new AccessDeniedException();
        validate(sessionGraph);
        final @Nullable UserGraph userGraph = serviceLocator.getAdminUserService().findById(sessionGraph.getUser().getId());
        if (userGraph == null) throw new AccessDeniedException();
        if (userGraph.getRole() != Role.ADMIN) throw new AccessDeniedException();
    }

    @Override
    @Nullable
    public SessionGraph close(@Nullable final SessionGraph sessionGraph) {
        if (sessionGraph == null) return null;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final SessionRepositoryGraph sessionRepository = new SessionRepositoryGraph(entityManager) ;
            sessionRepository.close(sessionGraph);
            entityManager.getTransaction().commit();
            return sessionGraph;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @Nullable
    public List<SessionGraph> findAll() {
        return null;
    }

    @Nullable
    public SessionGraph sign(@Nullable final SessionGraph sessionGraph) {
        if (sessionGraph == null) return null;
        sessionGraph.setSignature(null);
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @Nullable final String signature = HashUtil.salt(propertyService, sessionGraph);
        sessionGraph.setSignature(signature);
        return sessionGraph;
    }

}
