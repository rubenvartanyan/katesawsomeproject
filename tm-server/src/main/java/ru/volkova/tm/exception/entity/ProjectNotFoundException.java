package ru.volkova.tm.exception.entity;

import ru.volkova.tm.exception.AbstractException;

public class ProjectNotFoundException extends AbstractException {

    public ProjectNotFoundException() {
        super("Error! Project is not found...");
    }

}
