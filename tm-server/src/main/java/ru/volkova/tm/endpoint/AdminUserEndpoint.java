package ru.volkova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.endpoint.IAdminUserEndpoint;
import ru.volkova.tm.api.service.ServiceLocator;
import ru.volkova.tm.dto.Session;
import ru.volkova.tm.dto.User;
import ru.volkova.tm.enumerated.Role;
import ru.volkova.tm.model.UserGraph;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class AdminUserEndpoint extends AbstractEndpoint implements IAdminUserEndpoint {

    public AdminUserEndpoint(ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @WebMethod
    public void addUser(
            @Nullable @WebParam(name = "session", partName = "session") Session session,
            @NotNull @WebParam(name = "entity", partName = "entity") UserGraph entity
    ) {
        serviceLocator.getSessionDTOService().validateAdmin(session, Role.ADMIN);
        serviceLocator.getAdminUserService().add(entity);
    }

    @WebMethod
    public void clearUsers(@NotNull @WebParam(name = "session", partName = "session") Session session) {
        serviceLocator.getSessionDTOService().validateAdmin(session, Role.ADMIN);
        serviceLocator.getAdminUserService().clear();
    }

    @WebMethod
    public void createUserByLogPass(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "login", partName = "login") String login,
            @Nullable @WebParam(name = "password", partName = "password") String password
    ) {
        serviceLocator.getSessionDTOService().validateAdmin(session, Role.ADMIN);
        serviceLocator.getAdminUserService().createUser(login, password);
    }

    @WebMethod
    public void createUserWithEmail(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "login", partName = "login") String login,
            @Nullable @WebParam(name = "password", partName = "password") String password,
            @Nullable @WebParam(name = "email", partName = "email") String email
    ) {
        serviceLocator.getSessionDTOService().validateAdmin(session, Role.ADMIN);
        serviceLocator.getAdminUserService().createUserWithEmail(login, password, email);
    }

    @WebMethod
    public void createUserWithRole(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "login", partName = "login") String login,
            @Nullable @WebParam(name = "password", partName = "password") String password,
            @Nullable @WebParam(name = "role", partName = "role") Role role
    ) {
        serviceLocator.getSessionDTOService().validateAdmin(session, Role.ADMIN);
        serviceLocator.getAdminUserService().createUserWithRole(login, password, role);
    }

    @NotNull
    @WebMethod
    public List<User> findAllUser(
            @NotNull @WebParam(name = "session", partName = "session") Session session
    ) {
        serviceLocator.getSessionDTOService().validateAdmin(session, Role.ADMIN);
        return serviceLocator.getAdminUserDTOService().findAll();
    }

    @Nullable
    @WebMethod
    public User findUserById(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id
    ) {
        serviceLocator.getSessionDTOService().validateAdmin(session, Role.ADMIN);
        return serviceLocator.getAdminUserDTOService().findById(id);
    }

    @Nullable
    @WebMethod
    public User findUserByLogin(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "login", partName = "login") String login
    ) {
        serviceLocator.getSessionDTOService().validateAdmin(session, Role.ADMIN);
        return serviceLocator.getAdminUserDTOService().findByLogin(login);
    }

    @WebMethod
    public boolean isEmailExists(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "email", partName = "email") String email
    ) {
        serviceLocator.getSessionDTOService().validateAdmin(session, Role.ADMIN);
        return serviceLocator.getAdminUserService().isEmailExists(email);
    }

    @WebMethod
    public boolean isLoginExists(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "login", partName = "login") String login
    ) {
        serviceLocator.getSessionDTOService().validateAdmin(session, Role.ADMIN);
        return serviceLocator.getAdminUserService().isLoginExists(login);
    }

    @WebMethod
    public void lockByEmail(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "email", partName = "email") String email
    ) {
        serviceLocator.getSessionDTOService().validateAdmin(session, Role.ADMIN);
        serviceLocator.getAdminUserService().lockByEmail(email);
    }

    @WebMethod
    public void lockById(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id
    ) {
        serviceLocator.getSessionDTOService().validateAdmin(session, Role.ADMIN);
        serviceLocator.getAdminUserService().lockById(id);
    }

    @WebMethod
    public void lockByLogin(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "login", partName = "login") String login
    ) {
        serviceLocator.getSessionDTOService().validateAdmin(session, Role.ADMIN);
        serviceLocator.getAdminUserService().lockByLogin(login);
    }

    @WebMethod
    public void removeUserByEmail(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "email", partName = "email") String email
    ) {
        serviceLocator.getSessionDTOService().validateAdmin(session, Role.ADMIN);
        serviceLocator.getAdminUserService().removeByEmail(email);
    }

    @WebMethod
    public void removeUserById(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id
    ) {
        serviceLocator.getSessionDTOService().validateAdmin(session, Role.ADMIN);
        serviceLocator.getAdminUserService().removeById(id);
    }

    @WebMethod
    public void removeUserByLogin(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "login", partName = "login") String login
    ) {
        serviceLocator.getSessionDTOService().validateAdmin(session, Role.ADMIN);
        serviceLocator.getAdminUserService().removeByLogin(login);
    }


    @WebMethod
    public void unlockUserByEmail(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "email", partName = "email") String email
    ) {
        serviceLocator.getSessionDTOService().validateAdmin(session, Role.ADMIN);
        serviceLocator.getAdminUserService().unlockByEmail(email);
    }

    @WebMethod
    public void unlockUserById(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id
    ) {
        serviceLocator.getSessionDTOService().validateAdmin(session, Role.ADMIN);
        serviceLocator.getAdminUserService().unlockById(id);
    }

    @WebMethod
    public void unlockUserByLogin(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "login", partName = "login") String login
    ) {
        serviceLocator.getSessionDTOService().validateAdmin(session, Role.ADMIN);
        serviceLocator.getAdminUserService().unlockByLogin(login);
    }

    @WebMethod
    public void updateUser(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "firstName", partName = "firstName") String firstName,
            @Nullable @WebParam(name = "secondName", partName = "secondName") String secondName,
            @Nullable @WebParam(name = "middleName", partName = "middleName") String middleName
    ) {
        serviceLocator.getSessionDTOService().validateAdmin(session, Role.ADMIN);
        final String userId = session.getUser().getId();
        serviceLocator.getAdminUserService()
                .updateUser(userId, firstName, secondName, middleName);
    }

}
