package ru.volkova.tm.endpoint;

import ru.volkova.tm.api.service.ServiceLocator;

public abstract class AbstractEndpoint {

    final ServiceLocator serviceLocator;

    public AbstractEndpoint(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

}
