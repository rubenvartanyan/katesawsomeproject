package ru.volkova.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.endpoint.IProjectEndpoint;
import ru.volkova.tm.api.service.ServiceLocator;
import ru.volkova.tm.dto.Project;
import ru.volkova.tm.dto.Session;
import ru.volkova.tm.enumerated.Status;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @SneakyThrows
    @WebMethod
    public void changeProjectOneStatusById(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id,
            @Nullable @WebParam(name = "status", partName = "status") Status status
    ) {
        serviceLocator.getSessionDTOService().validate(session);
        serviceLocator.getProjectDTOService()
                .changeOneStatusById(session.getUser().getId(), id, status);
    }

    @SneakyThrows
    @WebMethod
    public void changeProjectStatusByName(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "name", partName = "name") String name,
            @Nullable @WebParam(name = "status", partName = "status") Status status
    ) {
        serviceLocator.getSessionDTOService().validate(session);
        serviceLocator.getProjectService()
                .changeOneStatusByName(session.getUser().getId(), name, status);
    }

    @WebMethod
    public void clearProject(
            @NotNull @WebParam(name = "session", partName = "session") Session session
    ) {
        serviceLocator.getSessionDTOService().validate(session);
        final String userId = session.getUser().getId();
        serviceLocator.getProjectService().clear(userId);
    }

    @SneakyThrows
    @NotNull
    @WebMethod
    public List<Project> findAllProjects(
            @NotNull @WebParam(name = "session", partName = "session") Session session
    ) {
        serviceLocator.getSessionDTOService().validate(session);
        final String userId = session.getUser().getId();
        return serviceLocator.getProjectDTOService().findAll(userId);
    }

    @Nullable
    @WebMethod
    public Project findProjectById(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @NotNull @WebParam(name = "id", partName = "id") String id
    ) {
        serviceLocator.getSessionDTOService().validate(session);
        final String userId = session.getUser().getId();
        return serviceLocator.getProjectDTOService().findById(userId, id);
    }

    @Nullable
    @WebMethod
    public Project findProjectByIndex(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "index", partName = "index") Integer index
    ) {
        serviceLocator.getSessionDTOService().validate(session);
        final String userId = session.getUser().getId();
        return serviceLocator.getProjectDTOService().findOneByIndex(userId, index);
    }

    @Nullable
    @WebMethod
    public Project findProjectByName(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "name", partName = "name") String name
    ) {
        serviceLocator.getSessionDTOService().validate(session);
        final String userId = session.getUser().getId();
        return serviceLocator.getProjectDTOService().findOneByName(userId, name);
    }

    @WebMethod
    public void removeProjectById(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @NotNull @WebParam(name = "id", partName = "id") String id
    ) {
        serviceLocator.getSessionDTOService().validate(session);
        final String userId = session.getUser().getId();
        serviceLocator.getProjectService().removeById(userId, id);
    }

    @WebMethod
    public void removeProjectByName(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "name", partName = "name") String name
    ) {
        serviceLocator.getSessionDTOService().validate(session);
        final String userId = session.getUser().getId();
        serviceLocator.getProjectService().removeOneByName(userId, name);
    }

    @WebMethod
    public void updateProjectById(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id,
            @Nullable @WebParam(name = "name", partName = "name") String name,
            @Nullable @WebParam(name = "description", partName = "description") String description
    ) {
        serviceLocator.getSessionDTOService().validate(session);
        final String userId = session.getUser().getId();
        serviceLocator.getProjectService()
                .updateOneById(userId, id, name, description);
    }

    @WebMethod
    public void addProjectByUser(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "name", partName = "name") String name,
            @Nullable @WebParam(name = "description", partName = "description") String description
    ) {
        serviceLocator.getSessionDTOService().validate(session);
        final String userId = session.getUser().getId();
        serviceLocator.getProjectService().add(userId, name, description);
    }

}
