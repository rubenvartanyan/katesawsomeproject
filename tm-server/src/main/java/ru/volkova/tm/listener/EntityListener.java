package ru.volkova.tm.listener;

import org.jetbrains.annotations.NotNull;

import javax.persistence.*;

import ru.volkova.tm.enumerated.OperationType;
import ru.volkova.tm.service.ActiveMQConnectionService;

import static ru.volkova.tm.enumerated.OperationType.*;


public final class EntityListener {

    @PostLoad
    public void onPostLoad(@NotNull final Object entity) {
        sendMessage(entity, LOAD);
    }

    @PrePersist
    public void onPrePersist(@NotNull final Object entity) {
        sendMessage(entity, START_PERSIST);
    }

    @PostPersist
    public void onPostPersist(@NotNull final Object entity) {
        sendMessage(entity, FINISH_PERSIST);
    }

    @PreUpdate
    public void onPreUpdate(@NotNull final Object entity) {
        sendMessage(entity, START_UPDATE);
    }

    @PostUpdate
    public void onPostUpdate(@NotNull final Object entity) {
        sendMessage(entity, FINISH_UPDATE);
    }

    @PreRemove
    public void onPreRemove(@NotNull final Object entity) {
        sendMessage(entity, START_REMOVE);
    }

    @PostRemove
    public void onPostRemove(@NotNull final Object entity) {
        sendMessage(entity, FINISH_REMOVE);
    }

    public void sendMessage(
            @NotNull final Object entity, @NotNull final OperationType operation
    ) {
        if (ActiveMQConnectionService.getInstance() == null) return;
        ActiveMQConnectionService.getInstance().getMessageService().sendAsync(entity, operation);
    }

}
