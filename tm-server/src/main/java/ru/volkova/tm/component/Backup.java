package ru.volkova.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.volkova.tm.bootstrap.Bootstrap;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Backup implements Runnable {

    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    public final Bootstrap bootstrap;

    private static final int INTERVAL = 30;

    public Backup (@NotNull Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void init() {
        load();
        start();
    }

    public void start() {
        es.scheduleWithFixedDelay(this, 0, INTERVAL, TimeUnit.SECONDS);
    }

    public void stop() {
        es.shutdown();
    }

    public void load() {
    }

    @Override
    public void run(){

    }

}
