package ru.volkova.tm.constant;

public interface TerminalConst {

    String CMD_VERSION = "version";

    String CMD_ABOUT = "about";

    String CMD_HELP = "help";

    String CMD_EXIT = "exit";

    String CMD_INFO = "info";

    String TASK_CREATE = "task-create";

    String TASK_CLEAR = "task-clear";

    String TASK_LIST = "task-list";

    String PROJECT_CREATE = "project-create";

    String PROJECT_CLEAR = "project-clear";

    String PROJECT_LIST = "project-list";

    String TASK_UPDATE_BY_INDEX = "task-update-by-index";

    String TASK_UPDATE_BY_ID = "task-update-by-id";

    String TASK_VIEW_BY_ID = "task-view-by-id";

    String TASK_VIEW_BY_NAME = "task-view-by-name";

    String TASK_VIEW_BY_INDEX = "task-view-by-index";

    String TASK_REMOVE_BY_ID = "task-remove-by-id";

    String TASK_REMOVE_BY_NAME = "task-remove-by-name";

    String TASK_REMOVE_BY_INDEX = "task-remove-by-index";

    String PROJECT_UPDATE_BY_INDEX = "project-update-by-index";

    String PROJECT_UPDATE_BY_ID = "project-update-by-id";

    String PROJECT_VIEW_BY_ID = "project-view-by-id";

    String PROJECT_VIEW_BY_NAME = "project-view-by-name";

    String PROJECT_VIEW_BY_INDEX = "project-view-by-index";

    String PROJECT_REMOVE_BY_ID = "project-remove-by-id";

    String PROJECT_REMOVE_BY_NAME = "project-remove-by-name";

    String PROJECT_REMOVE_BY_INDEX = "project-remove-by-index";

    String PROJECT_START_BY_ID = "project-start-by-id";

    String PROJECT_START_BY_NAME = "project-start-by-name";

    String PROJECT_START_BY_INDEX = "project-start-by-index";

    String PROJECT_FINISH_BY_ID = "project-finish-by-id";

    String PROJECT_FINISH_BY_NAME = "project-finish-by-name";

    String PROJECT_FINISH_BY_INDEX = "project-finish-by-index";

    String PROJECT_CHANGE_STATUS_BY_ID = "project-change-status-by-id";

    String PROJECT_CHANGE_STATUS_BY_NAME = "project-change-status-by-name";

    String PROJECT_CHANGE_STATUS_BY_INDEX = "project-change-status-by-index";

    String PROJECT_REMOVE_BY_ID_CASCADE = "project-remove-by-id-cascade";

    String TASK_START_BY_ID = "task-start-by-id";

    String TASK_START_BY_NAME = "task-start-by-name";

    String TASK_START_BY_INDEX = "task-start-by-index";

    String TASK_FINISH_BY_ID = "task-finish-by-id";

    String TASK_FINISH_BY_NAME = "task-finish-by-name";

    String TASK_FINISH_BY_INDEX = "task-finish-by-index";

    String TASK_CHANGE_STATUS_BY_ID = "task-change-status-by-id";

    String TASK_CHANGE_STATUS_BY_NAME = "task-change-status-by-name";

    String TASK_CHANGE_STATUS_BY_INDEX = "task-change-status-by-index";

    String TASK_FIND_ALL_BY_PROJECT_ID = "task-find-all-by-project-id";

    String TASK_BIND_BY_PROJECT_ID = "task-bind-by-project-id";

    String TASK_UNBIND_BY_PROJECT_ID = "task-unbind-by-project-id";

    String CMD_COMMANDS = "commands";

    String CMD_ARGUMENTS = "arguments";

}
