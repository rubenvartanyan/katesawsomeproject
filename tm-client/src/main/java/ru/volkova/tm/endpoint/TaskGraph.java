
package ru.volkova.tm.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Java class for taskGraph complex type.
 * 
 * &lt;p&gt;The following schema fragment specifies the expected content contained within this class.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="taskGraph"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;extension base="{http://endpoint.tm.volkova.ru/}abstractOwnerEntityGraph"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="project" type="{http://endpoint.tm.volkova.ru/}projectGraph" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/extension&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "taskGraph", propOrder = {
    "project"
})
public class TaskGraph
    extends AbstractOwnerEntityGraph
{

    protected ProjectGraph project;

    /**
     * Gets the value of the project property.
     * 
     * @return
     *     possible object is
     *     {@link ProjectGraph }
     *     
     */
    public ProjectGraph getProject() {
        return project;
    }

    /**
     * Sets the value of the project property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProjectGraph }
     *     
     */
    public void setProject(ProjectGraph value) {
        this.project = value;
    }

}
