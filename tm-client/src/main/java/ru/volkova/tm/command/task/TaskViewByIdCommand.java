package ru.volkova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.endpoint.Session;
import ru.volkova.tm.endpoint.Role;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;
import ru.volkova.tm.endpoint.Task;
import ru.volkova.tm.util.TerminalUtil;

import java.util.Optional;

public class TaskViewByIdCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "find task by id";
    }

    @Override
    public void execute() {
        if (bootstrap == null) throw new ObjectNotFoundException();
        if (endpointLocator == null) throw new ObjectNotFoundException();
        @Nullable final Session session = bootstrap.getSession();
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        @NotNull final Task task = endpointLocator.getTaskEndpoint().findTaskById(session, id);
        showTask(task);
    }

    @NotNull
    @Override
    public String name() {
        return "task-view-by-id";
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
