package ru.volkova.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.command.AbstractCommand;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;

import java.util.Collection;

public class ArgumentsListCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "show program arguments";
    }

    @Override
    public void execute() {
        if (endpointLocator == null) throw new ObjectNotFoundException();
        System.out.println("[ARGUMENTS]");
        @NotNull final Collection<AbstractCommand> commands = bootstrap.getCommandService().getCommands();
        for (final AbstractCommand command : commands) {
            final String arg = command.arg();
            if (arg == null) continue;
            System.out.println(arg);
        }
    }

    @NotNull
    @Override
    public String name() {
        return "arguments-list";
    }

}
