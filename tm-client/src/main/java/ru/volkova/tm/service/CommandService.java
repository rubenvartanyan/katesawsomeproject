package ru.volkova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.command.AbstractCommand;
import ru.volkova.tm.repository.CommandRepository;

import java.util.Collection;

public class CommandService {

    @NotNull
    private final CommandRepository commandRepository;

    public CommandService(@NotNull CommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    public void add(@Nullable AbstractCommand command) {
        if (command == null) return;
        commandRepository.add(command);
    }

    @NotNull
    public Collection<AbstractCommand> getArguments() {
        return commandRepository.getArguments();
    }

    @NotNull
    public Collection<AbstractCommand> getArgsCommandsList() {
        return commandRepository.getArgsCommandsList();
    }

    @Nullable
    public AbstractCommand getCommandByArg(@Nullable final String arg) {
        if (arg == null || arg.isEmpty()) return null;
        return commandRepository.getCommandByArg(arg);
    }

    @Nullable
    public AbstractCommand getCommandByName(@Nullable final String name) {
        if (name == null || name.isEmpty()) return null;
        return commandRepository.getCommandByName(name);
    }

    @NotNull
    public Collection<AbstractCommand> getCommands() {
        return commandRepository.getCommands();
    }


}
